# python excercise 42

class Animal(object):
    pass    

## is a relation with class animal
class Dog(Animal):
    def __init__(self, name):
    ## 
    self.name = name
    

## is a relation with class Animal
class Cat(Animal):
    def __init__(self, name):
    ## ??
    self.name = name


## is a relation with object class 
class Person(object):
    def __init__(self, name):
    ## ??
    self.name = name
    ## Person has- a pet of some kind
    self.pet = None

## is a relation with Person class
class Employee(Person):
    def __init__(self, name, salary):
    ## ?? hmm what is this strange magic?
    super(Employee, self).__init__(name)
    ## ??
    self.salary = salary


## ??
class Fish(object):
    pass


## ??
class Salmon(Fish):
    pass


## ??
class Halibut(Fish):
    pass


## rover is- a Dog
rover = Dog("Rover")
## ??
satan = Cat("Satan")
## ??
mary = Person("Mary")
## ??
mary.pet = satan
## ??
frank = Employee("Frank", 120000)
65
## ??
frank.pet = rover
## ??
flipper = Fish()
crouse = Salmon()

## ??
harry = Halibut()
